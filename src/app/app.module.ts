import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AppProvider } from '../providers/app/app';
import { HTTP } from "@ionic-native/http";
import { Media } from "@ionic-native/media";
import { YoutubeVideoPlayer } from "@ionic-native/youtube-video-player";
import { SocialSharing } from "@ionic-native/social-sharing";
@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
      backButtonIcon: 'arrow-back',
      iconMode: 'md',
      tabsPlacement:'top',
      tabsHighlight:'true',
      tabsLayout:'icon-hide'
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AppProvider,
    YoutubeVideoPlayer,
    Media,
    HTTP,
    SocialSharing
  ]
})
export class AppModule {}
