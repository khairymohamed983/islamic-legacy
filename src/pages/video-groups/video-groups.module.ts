import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoGroupsPage } from './video-groups';

@NgModule({
  declarations: [
    VideoGroupsPage,
  ],
  imports: [
    IonicPageModule.forChild(VideoGroupsPage),
  ],
})
export class VideoGroupsPageModule {}
