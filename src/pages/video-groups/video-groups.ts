import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HTTP } from "@ionic-native/http";

@IonicPage()
@Component({
  selector: 'page-video-groups',
  templateUrl: 'video-groups.html',
})
export class VideoGroupsPage {
  
  isListView = true;
  isLoaded = false;
  list = []; 
  constructor(public navCtrl: NavController, public http: HTTP, public navParams: NavParams) {
    this.isLoaded = false;
    this.http.get('http://alsehamy.net/eslamy/api/videogroub', {}, {}).then((data) => {
      this.isLoaded = true;
      this.list = JSON.parse(data.data).data;
    }).catch((ex) => {
      this.isLoaded = true;
      console.log(ex);
    });
  } 
  
  toggleView() {
    this.isListView = !this.isListView;
  }

  expendGroup(group){
    if(group.main){
      this.navCtrl.parent.parent.push('VideoSeriesPage', {dept : group.id , title:group.name});      
    }else{
      this.navCtrl.parent.parent.push('SubjectsListPage', {dept : group.id , title:group.name});
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VideoGroupsPage');
  }

}
