import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PageTypes } from "../../providers/pageTypes";
@IonicPage()
@Component({
  selector: 'page-videos',
  templateUrl: 'videos.html',
})
export class VideosPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  } 

  groups = {
      id : 0,
      type:PageTypes.Groups,
      title:'المجموعة' , 
      source:1
    }
    
    sd = {
      id : 1,    
      type:PageTypes.Series,
      title:'السلسلة',
      source:1    
    }
  
    subjects = {
      id : 2 ,    
      type:PageTypes.Basic,
      title:'المادة',
      source:1    
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VideosPage');
  }
}
