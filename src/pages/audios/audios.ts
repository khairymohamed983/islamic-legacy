import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PageTypes } from '../../providers/pageTypes';
 
@IonicPage()
@Component({
  selector: 'page-audios',
  templateUrl: 'audios.html',
})
export class AudiosPage {
 
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  series = {
    id : 1,    
    type:PageTypes.Series,
    title:'السلسلة',
    source:1    
  }

  subjects = {
    id : 2 ,    
    type:PageTypes.Basic,
    title:'المادة',
    source:1    
  }

  ionViewDidLoad() { 
 
    console.log('ionViewDidLoad AudiosPage');
  }

}
