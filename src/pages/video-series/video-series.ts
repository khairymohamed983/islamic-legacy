import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';

@IonicPage()
@Component({
  selector: 'page-video-series',
  templateUrl: 'video-series.html',
})
export class VideoSeriesPage {

  isLoaded = false;
  list = []; 
  deptTitle: string;

  constructor(public navCtrl: NavController, public http: HTTP, public navParams: NavParams) {
    this.isLoaded = false;
    let dept = navParams.get('dept');
    this.deptTitle = navParams.get('title');
    let url = 'http://alsehamy.net/eslamy/api/videodeps'
    if (dept) {
      url +=`?groub=${dept}`;
    }
    console.log(url);
    this.http.get(url, {}, {}).then((data) => {
      this.isLoaded = true;
      this.list = JSON.parse(data.data).data;
    }).catch((ex) => {
      this.isLoaded = true;
      console.log(ex);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VideoSeriesPage');
  }
  
  expandSeries(series){
    if(this.navCtrl.parent){
      this.navCtrl.parent.parent.push('SubjectsListPage', {dept : series.id , title:series.name});
    }else{
      this.navCtrl.push('SubjectsListPage', {dept : series.id , title:series.name});
    }
  }
  
  isListView = true;
  toggleView(){
    this.isListView = !this.isListView;
  }

}
