import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoSeriesPage } from './video-series';

@NgModule({
  declarations: [
    VideoSeriesPage,
  ],
  imports: [
    IonicPageModule.forChild(VideoSeriesPage),
  ],
})
export class VideoSeriesPageModule {}
