import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HTTP } from "@ionic-native/http";

@IonicPage()
@Component({
  selector: 'page-audio-list',
  templateUrl: 'audio-list.html',
})
export class AudioListPage {

  deptTitle: string;
  list = [];
  isLoaded = false;
  constructor(public navCtrl: NavController,
    public http: HTTP,
    public navParams: NavParams) {
    debugger;
    this.isLoaded = false;
    let dept = navParams.get('dept');
    this.deptTitle = navParams.get('title');
    let url = 'http://alsehamy.net/eslamy/api/audio'
    if (dept) {
      url += `?dep_id=${dept}`;
    }
    this.http.get(url, {}, {}).then((data) => {
      this.isLoaded = true;
      this.list = JSON.parse(data.data).data;
    }).catch((exec) => {
      console.log(exec);
      this.isLoaded = true;
    })
  }

  isListView = true;
  
  toggleView() {
    this.isListView = !this.isListView;
  }

  openAudioDetails(audio) {
    if (this.navCtrl.parent) {
      this.navCtrl.parent.parent.push('SubjectDetailPage', {
        title: audio.name,//item.title,
        source: 0,
        file: audio.path,
        description: audio.text, //item.description,
        sheikh: 'فضيلة الشيخ / نبيل العوضي', // item.sheikh,
        visits: audio.views,  //item.visits,
        // size: '150 ميجا بايت' // item.size
      })
    } else {
      this.navCtrl.push('SubjectDetailPage', {
        title: audio.name,//item.title,
        source: 0,
        file: audio.path,
        description: audio.text, //item.description,
        sheikh: 'فضيلة الشيخ / نبيل العوضي', // item.sheikh,
        visits: audio.views,  //item.visits,
        // size: '150 ميجا بايت' // item.size
      })
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AudioListPage');
  }
}
