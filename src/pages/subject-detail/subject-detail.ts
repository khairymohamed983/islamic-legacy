import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, TabHighlight } from 'ionic-angular';
import { YoutubeVideoPlayer } from "@ionic-native/youtube-video-player";
import { SocialSharing } from '@ionic-native/social-sharing';
@IonicPage()
@Component({
  selector: 'page-subject-detail',
  templateUrl: 'subject-detail.html',
})
export class SubjectDetailPage {

  videoId : string;
  audioFIle :string;
  subjectTitle: string = '';
  source: number;
  description: string = '';
  sheikh: string = '';
  visits: number = 0;
  // size: string = '';
  fullVideoUrl :string;
  isAudio: boolean = false;
  constructor(public navCtrl: NavController,
    private actionSheetCtrl :ActionSheetController,
    public socialSharing:SocialSharing,
    public navParams: NavParams , private youtube :YoutubeVideoPlayer) {
    this.subjectTitle = navParams.get('title');
    this.source = navParams.get('source');
    this.description = navParams.get('description');
    this.sheikh = navParams.get('sheikh');
    this.audioFIle = navParams.get('file');
    this.fullVideoUrl = navParams.get('video');
    let vurl = this.fullVideoUrl;
    if(vurl){
      this.videoId =vurl.split('=')[1];
    }
    this.visits = navParams.get('visits');
    // this.size = navParams.get('size');
    if (this.source == 0) {
      this.isAudio = true;
    }
  }
 
  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'المشاركة عبر وسائل التواصل الإجتماعي',
      buttons: [
        {
          text: 'فيس بوك',
          role: 'facebook',
          icon:'logo-facebook',
          handler: () => {
            console.log('facebook clicked');
            this.socialSharing.shareViaFacebook(this.subjectTitle , null , this.isAudio ? this.audioFIle : this.fullVideoUrl)
          }
        },
        {
          text: 'تويتر',
          role: 'twitter',
          icon:'logo-twitter',
          handler: () => {
            console.log('twiter clicked');
            this.socialSharing.shareViaTwitter(this.subjectTitle , null , this.isAudio ? this.audioFIle : this.fullVideoUrl)
            
          }
        },
        {
          text: 'واتساب',
          role: 'whatsup',
          icon:'logo-whatsapp',
          handler: () => {
            console.log('whats clicked');
            this.socialSharing.shareViaWhatsApp(this.subjectTitle , null , this.isAudio ? this.audioFIle : this.fullVideoUrl)            
          }
        },
        {
          text: 'إلغاء',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
 
  playVideo() {
    this.youtube.openVideo(this.videoId);
  }

  listen(){
    this.navCtrl.push('AudioPlayerPage',{
      title:this.subjectTitle,
      file: this.audioFIle
    });
  }
  
  playAudio() {
    this.navCtrl.push('AudioPlayerPage', {
      title: this.subjectTitle,
      file: this.audioFIle
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubjectDetailPage');
  }

}
