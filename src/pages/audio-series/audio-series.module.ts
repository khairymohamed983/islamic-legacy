import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AudioSeriesPage } from './audio-series';

@NgModule({
  declarations: [
    AudioSeriesPage,
  ],
  imports: [
    IonicPageModule.forChild(AudioSeriesPage),
  ],
})
export class AudioSeriesPageModule {}
