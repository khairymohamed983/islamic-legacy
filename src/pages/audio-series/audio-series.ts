import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HTTP } from "@ionic-native/http";

@IonicPage()
@Component({
  selector: 'page-audio-series',
  templateUrl: 'audio-series.html',
})
export class AudioSeriesPage {

  list = [];
  isListView = true;
  deptTitle: string;

  isLoaded = false;
  constructor(public navCtrl: NavController,
    public http: HTTP
    , public navParams: NavParams) {


    this.isLoaded = false;
    let dept = navParams.get('dept');
    this.deptTitle = navParams.get('title');
    let url = 'http://alsehamy.net/eslamy/api/audiodeps'
    if (dept) {
      url += `?groub=${dept}`;
    }
    console.log(url);
    this.http.get(url, {}, {}).then((data) => {
      this.isLoaded = true;
      this.list = JSON.parse(data.data).data;
    }).catch((ex) => {
      this.isLoaded = true;
      console.log(ex);
    });
  }

  expandSeries(series) {
    if (this.navCtrl.parent) {
      this.navCtrl.parent.parent.push('AudioListPage', { dept: series.id, title: series.name });
    } else {
      this.navCtrl.push('AudioListPage', { dept: series.id, title: series.name });
    }
  }

  toggleView() {
    this.isListView = !this.isListView;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AudioSeriesPage');
  }

}
