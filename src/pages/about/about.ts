import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  content: string;
  isLoaded = false;
  constructor(public navCtrl: NavController,
    public http: HTTP,
    public navParams: NavParams) {
    this.isLoaded = false;
    this.http.get('http://alsehamy.net/eslamy/api/pages/single/2', {}, {}).then(data => {
      this.isLoaded = true;
      this.content = JSON.parse(data.data).data.text;
    }).catch(ex => {
      this.isLoaded = true;
      console.log(ex);
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }

}
