import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PageTypes } from "../../providers/pageTypes";
import { HTTP } from '@ionic-native/http';

@IonicPage()

@Component({
  selector: 'page-subjects-list',
  templateUrl: 'subjects-list.html',
})

export class SubjectsListPage {

  isListView = true;
  list = [];
  isLoaded = false;
  deptTitle: string;

  constructor(public navCtrl: NavController,
    public http: HTTP,
    public navParams: NavParams) {
    this.isLoaded = false;
    let dept = navParams.get('dept');
    this.deptTitle = navParams.get('title');
    let url = 'http://alsehamy.net/eslamy/api/video'
    if (dept) {
      url +=`?dep_id=${dept}`;
    }
    console.log(url);
    
    this.http.get(url, {}, {}).then((data) => {
      this.isLoaded = true;
      this.list = JSON.parse(data.data).data;
    }).catch((exec) => {
      console.log(exec);
      this.isLoaded = true;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubjectsListPage');
  }

  toggleView() {
    this.isListView = !this.isListView;
  }

  navigateToItem(item) {
    console.log(item);
    if(this.navCtrl.parent){
      this.navCtrl.parent.parent.push('SubjectDetailPage', {
        title: item.name,//item.title,
        source:1,
        video: item.url,
        description: item.text, //item.description,
        sheikh: 'فضيلة الشيخ / نبيل العوضي', // item.sheikh,
        visits: item.views,  //item.visits,
      })
    }else{
      this.navCtrl.push('SubjectDetailPage', {
        title: item.name,//item.title,
        source:1,
        video: item.url,
        description: item.text, //item.description,
        sheikh: 'فضيلة الشيخ / نبيل العوضي', // item.sheikh,
        visits: item.views,  //item.visits,
      })
    } 
  }
}
