import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubjectsListPage } from './subjects-list';

@NgModule({
  declarations: [
    SubjectsListPage,
  ],
  imports: [
    IonicPageModule.forChild(SubjectsListPage),
  ],
  entryComponents:[
    SubjectsListPage
  ]
})
export class SubjectsListPageModule {}
