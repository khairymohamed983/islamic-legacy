import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AudioGroubPage } from './audio-groub';

@NgModule({
  declarations: [
    AudioGroubPage,
  ],
  imports: [
    IonicPageModule.forChild(AudioGroubPage),
  ]
})
export class AudioGroubPageModule {}
