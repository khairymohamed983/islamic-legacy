import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Media, MediaObject } from '@ionic-native/media';

@IonicPage()
@Component({
  selector: 'page-audio-player',
  templateUrl: 'audio-player.html',
})
export class AudioPlayerPage {

  title: string = '';
  fileName: string = '';
  isPlaying = false;
  file: MediaObject = null;
  constructor(public navCtrl: NavController, public navParams: NavParams, private media: Media) {
    this.title = this.navParams.get('title');
    this.fileName = this.navParams.get('file');
    this.file = this.media.create(this.fileName);
  }
  
  ionViewWillLeave(){
    this.file.stop();
  }
  triggerPlay() {
    if (this.isPlaying) {
      this.file.pause();
      this.isPlaying = false;
    } else {
      this.file.play();
      this.isPlaying = true;
    }
  }

  forward() {
    this.file.getCurrentPosition().then((position) => {
      if ((this.file.getDuration() * 1000 ) - position < 100) {
        this.file.seekTo(0);
      }else{
        this.file.seekTo(position - 100);
      }
    });
  }

  backward() {
    this.file.getCurrentPosition().then((position) => {
      if (position < 100) {
        this.file.seekTo(0);
      }else{
        this.file.seekTo(position - 100);
      }
    });
  }

  ionViewDidLoad() {

  }
}
